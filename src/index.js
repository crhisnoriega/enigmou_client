import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";

import "./styles.css";

const bcrypt = require("bcryptjs");

function doCall() {
  var hash = bcrypt.hashSync("@Enigmou#rocks");
 
  console.log(hash);
  
  axios
    .post(
      "/api/login",
      { login: "batman", password: "selina", devicetype: "0" },
      {
        headers: {
          "Content-Type": "application/json",
          token: hash
        }
      }
    )
    .then(response => {
      alert(JSON.stringify(response.data));
      console.log(response.data);
      return JSON.stringify(response.data);
    })
    .catch(error => {
      console.log(error);
    });
}

class MyApp extends React.Component {
  state = { result:"Clique no botao" };

  doCall() {
    var hash = bcrypt.hashSync("@Enigmou#rocks");
    console.log(hash);
    
    axios
      .post(
        "/api/login",
        { login: "batman", password: "selina", devicetype: "0" },
        {
          headers: {
            "Content-Type": "application/json",
            "token": hash
          }
        }
      )
  
      .then(response => {
        console.log(response.data);      
        this.setState({result: JSON.stringify(response.data)});
      })
      .catch(error => {
        console.log(error.response);
        this.setState({result: JSON.stringify(error.response)});      
        console.log(error);
      });
  }
  
  handleClick(){
    this.setState({result: "Calling..."});
    this.doCall();   
  };

  onChange() {
  }
  
  render() {
    return (
      <div>
        <input type="text" style={{width:"100%"}} value={this.state.result} />
        <button onClick={(e) => {this.handleClick(e)}}>
           call Enigmou API
        </button>
      </div>
    );
  }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<MyApp />, rootElement);
